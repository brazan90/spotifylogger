# README #

### What is this repository for? ###

Runs a console application that connects to Spotify through their Web API. It then saves listened to tracks to a local database. Currently SQLExpress is used.
Note that the app cannot generate an access or refresh token itself (for the Spotify API), you will have to get them yourself. It can however handle refreshing an access token. Some more information on this below.

### How do I get set up? ###

Access/Refresh tokens:

1. Download Postman or similar program
2. Create an app on Spotify's developer page ( https://developer.spotify.com/my-applications )
3. On the settings page for your app, enter "http://localhost/callback" as the Redirect URI
4. Change clientID on below URI and visit it: 
https://accounts.spotify.com/authorize/?client_id=YOUR-CLIENT-ID&response_type=code&redirect_uri=http://localhost/callback&scope=user-read-recently-played%20user-read-currently-playing . 
Since we are not hosting a page for the redirect URI, you will be sent to a page that does not exists. Copy the code you see in the URI for this page.
5. Use postman to complete the process detailed here: https://developer.spotify.com/web-api/authorization-guide/#authorization_code_flow . You can begin at step 4. Make sure you send a header that specifies the Content-Type as: application/x-www-form-urlencoded .
6. You should now get an access and refresh token. Enter all data into the database.

Database:
You can see how to Model the database by looking at the Model-folder in the project.

### Contribution guidelines ###

Nothing yet..

### Who do I talk to? ###

Repo Owner