﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace SpotifyLogger
{
    class Program
    {
        private static readonly AppSettings AppSettings = new AppSettings();

        static void Main(string[] args)
        {
            InitSettings();

            var spotifyController = new SpotifyController();
            var excelController = new ExcelController();

            if (spotifyController.TokenExpired())
            {
                spotifyController.RefreshToken();
            }

            spotifyController.GetLatestListenedTo();

            excelController.CreateDocument(spotifyController.GetAllListenedTracks());
        }

        private static void InitSettings()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

            config.GetSection("AppSettings").Bind(AppSettings);
        }
    }
}
