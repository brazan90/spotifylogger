﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OfficeOpenXml;
using SpotifyLogger.Model;


namespace SpotifyLogger
{
    class ExcelController
    {
        public ExcelController()
        {
            
        }

        public void CreateDocument(List<ListenedToInstance> tracks)
        {
            string filePath = AppSettings.ExcelFilePath;
            var targetFile = new FileInfo(filePath);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (var excelFile = new ExcelPackage(targetFile))
            {
                var worksheet = excelFile.Workbook.Worksheets.Add("Sheet1");
                for (var i = 0; i < tracks.Count; i++)
                {
                    worksheet.Cells[i + 2, 1].Value = tracks[i].played_at;
                    worksheet.Cells[i + 2, 2].Value = tracks[i].track.name;
                }
                worksheet.Cells[2, 1, tracks.Count + 1, 1].Style.Numberformat.Format = "yyyy-mm-dd HH:mm:ss";
                //worksheet.Cells["A1"].LoadFromCollection(Collection: tracks, PrintHeaders: true);
                excelFile.SaveAs(targetFile);
            }
        }
    }
}
