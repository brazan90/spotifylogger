﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyLogger
{
    class AppSettings
    {
        public static string ConnectionString { get; set; }
        public static string ExcelFilePath { get; set; }
        public static SpotifySettings SpotifySettings { get; set; }
    }

    class SpotifySettings
    {
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
    }
}
