﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyLogger.Model
{
    public class SpotifyListenedTrack
    {
        public List<ListenedToInstance> items { get; set; }
    }

    public class ListenedToInstance
    {
        public Track track { get; set; }
        public DateTime played_at { get; set; }
    }

    public class Artist
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Image
    {
        public string url { get; set; }
    }

    public class Album
    {
        public string id { get; set; }
        public List<Image> images { get; set; }
        public string name { get; set; }
    }

    public class Track
    {
        public Album album { get; set; }
        public List<Artist> artists { get; set; }
        public int duration_ms { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
}
