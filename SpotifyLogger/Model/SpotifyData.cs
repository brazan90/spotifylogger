﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpotifyLogger.Model
{
    class SpotifyData
    {
        public int ID { get; set; }
        public string Access_token { get; set; }
        public string Refresh_token { get; set; }
        public string Expires_in { get; set; }
        public string Token_type { get; set; }
        public string Scope { get; set; }
        public DateTime Created_at { get; set; }
    }
}
