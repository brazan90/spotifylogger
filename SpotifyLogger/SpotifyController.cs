﻿using SpotifyLogger.Model;
using SpotifyLogger.Repository;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace SpotifyLogger
{
    class SpotifyController
    {
        private readonly SpotifyData _spotifyData;
        private readonly SpotifyDataRepository _spotifyDataRepository;
        private const string RefreshUri = "https://accounts.spotify.com/api/token";
        private const string GetLatestUri = "https://api.spotify.com/v1/me/player/recently-played";
        private const string TrackLimitUri = "50";

        public SpotifyController()
        {
            _spotifyDataRepository = new SpotifyDataRepository();
            _spotifyData = _spotifyDataRepository.GetSpotifyData();
        }

        public bool TokenExpired()
        {
            var tokenExpiration = _spotifyData.Created_at.AddSeconds(Int32.Parse(_spotifyData.Expires_in));
            if (DateTime.Now >= tokenExpiration)
            {
                return true;
            }
            return false;
        }

        public void RefreshToken()
        {
            var webRequest = WebRequest.Create(RefreshUri);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Headers.Add("Authorization", "Basic " + Base64EncodedAuthCode());
            var data = "grant_type=refresh_token";
            data += "&refresh_token=" + _spotifyData.Refresh_token;
            var postData = Encoding.UTF8.GetBytes(data);
            webRequest.ContentLength = postData.Length;

            using (var stream = webRequest.GetRequestStream())
            {
                stream.Write(postData, 0, postData.Length);
            }

            var response = (HttpWebResponse)webRequest.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            var rtToken = JsonConvert.DeserializeObject<RootObject>(responseString);

            _spotifyData.Access_token = rtToken.access_token;
            _spotifyData.Created_at = DateTime.Now;
            _spotifyDataRepository.UpdateAccessToken(_spotifyData);
        }

        public void GetLatestListenedTo()
        {
            SpotifyListenedTrack tracks;
            var webRequest = WebRequest.Create(GetLatestUri + "?limit=" + TrackLimitUri);
            webRequest.Method = "GET";
            webRequest.Headers.Add("Authorization", "Bearer " + _spotifyData.Access_token);

            var response = (HttpWebResponse) webRequest.GetResponse();
            using (var responseString = new StreamReader(response.GetResponseStream()))
            {
                var json = responseString.ReadToEnd();
                tracks = JsonConvert.DeserializeObject<SpotifyListenedTrack>(json);
            }
            
            var latestListenedDate = _spotifyDataRepository.getLatestListenedDate();
            foreach (var track in tracks.items)
            {
                if (track.played_at > latestListenedDate)
                {
                    _spotifyDataRepository.InsertListenedTrack(track);
                }
            }
        }

        public List<ListenedToInstance> GetAllListenedTracks()
        {
            return _spotifyDataRepository.GetAllListenedTracks();
        }

        private string Base64EncodedAuthCode()
        {
            var encodedText = Encoding.UTF8.GetBytes(SpotifySettings.ClientId + ":" + SpotifySettings.ClientSecret);
            return Convert.ToBase64String(encodedText);
        }

        private long GetUnixTimeStamp(DateTime convertDate)
        {
            return ((DateTimeOffset)convertDate).ToUnixTimeMilliseconds();
        }
    }
}
