﻿using SpotifyLogger.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Linq;
using Dapper;
using Newtonsoft.Json;

namespace SpotifyLogger.Repository
{
    class SpotifyDataRepository
    {
        private readonly string _tableName = "SpotifyData";

        public SpotifyDataRepository()
        {

        }

        private IDbConnection Connection
        {
            get
            {
                return new SqlConnection(AppSettings.ConnectionString);
            }
        }

        public SpotifyData GetSpotifyData()
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = $"SELECT * FROM {_tableName}";
                dbConnection.Open();
                return dbConnection.Query<SpotifyData>(sQuery).FirstOrDefault();
            }
        }

        public void UpdateAccessToken(SpotifyData spotifyObj)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = $"UPDATE {_tableName} SET access_token = @Access_token, created_at = @Created_at WHERE ID = @ID";
                dbConnection.Open();
                var result = dbConnection.Execute(sQuery, spotifyObj);
                Console.WriteLine(DateTime.Now + ": New Token has been saved to database.");
            }
        }

        public DateTime getLatestListenedDate()
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "SELECT top(1) * from ListenedTracks order by played_at desc";
                dbConnection.Open();
                var t = dbConnection.Query<ListenedToInstance>(sQuery).FirstOrDefault();
                return t == null ? new DateTime(1500,01,01) : t.played_at;
            }
        }

        public void InsertListenedTrack(ListenedToInstance spotifyObj)
        {
            using (IDbConnection dbConnection = Connection)
            {
                var ArtistNames = "";
                var ArtistIds = "";
                for (var i = 0; i < spotifyObj.track.artists.Count; i++)
                {
                    if (i == spotifyObj.track.artists.Count-1)
                    {
                        ArtistNames += spotifyObj.track.artists[i].name;
                        ArtistIds += spotifyObj.track.artists[i].id;
                    }
                    else
                    {
                        ArtistNames += spotifyObj.track.artists[i].name + ",";
                        ArtistIds += spotifyObj.track.artists[i].id + ",";
                    }
                    
                }
                var sQuery =
                    @"INSERT into ListenedTracks(Played_at,Artist_id,Artist_name,Track_duration,Track_name,Track_id,Album_id,Album_name,Album_imagelink) 
                    values(@played_at,@artistID,@artistName,@duration_ms,@trackName,@trackId,@albumId,@albumName,@imageUrl)";
                dbConnection.Open();
                dbConnection.Execute(sQuery, 
                    new
                    {
                        spotifyObj.played_at,
                        artistID = ArtistIds,
                        artistName = ArtistNames,
                        spotifyObj.track.duration_ms,
                        trackName = spotifyObj.track.name,
                        trackId = spotifyObj.track.id,
                        albumId = spotifyObj.track.album.id,
                        albumName = spotifyObj.track.album.name,
                        imageUrl = spotifyObj.track.album.images[0].url
                    });
            }
        }

        public List<ListenedToInstance> GetAllListenedTracks()
        {
            using (IDbConnection dbConnection = Connection)
            {
                var t = dbConnection.Query<ListenedToInstance, Track, Album, Image, Artist, ListenedToInstance>(
                    @"SELECT Played_at, 
                    Track_id as id, Track_name as name, Track_duration as duration_ms,
                    Album_id as id, Album_name as name,
                    Album_imagelink as url,
                    Artist_id as id, Artist_name as name
                    FROM ListenedTracks order by played_at asc", 
                    (listenedToInstance, track, album, image, artist) => {
                        listenedToInstance.track = track;
                        listenedToInstance.track.album = album;
                        listenedToInstance.track.album.images = new List<Image>();
                        listenedToInstance.track.album.images.Add(image);
                        listenedToInstance.track.artists = new List<Artist>();
                        listenedToInstance.track.artists.Add(artist);
                        return listenedToInstance;
                    }, splitOn: "id, id,url,id").AsQueryable().ToList();

                return t;
            }
        }
    }
}
